package cs.mad.rpggame.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import cs.mad.rpggame.R
import cs.mad.rpggame.adapters.LeaderBoardScreenAdapter
import cs.mad.rpggame.adapters.LoginScreenAdapter
import cs.mad.rpggame.database.UserDatabase
import cs.mad.rpggame.databinding.ActivityLeaderBoardScreenBinding
import cs.mad.rpggame.databinding.ActivityLoginScreenBinding
import cs.mad.rpggame.entities.UserDAO
import cs.mad.rpggame.runOnIO

class LeaderBoardScreen : AppCompatActivity() {
    private lateinit var userdao: UserDAO
    private lateinit var binding: ActivityLeaderBoardScreenBinding

    // any time you want to use database within this class or adapter
    // use runOnIO {} and place code within brackets

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeaderBoardScreenBinding.inflate(layoutInflater)
//        setContentView(R.layout.activity_leader_board_screen)


        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            UserDatabase::class.java, UserDatabase.databaseName
        ).build()

        userdao = db.userDao()

        //Enter empty list with dao
        binding.leaderBoardList.adapter = LeaderBoardScreenAdapter(listOf(), userdao)

        // update that list within adapter through this function
        loadData()


        binding.home.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }


    private fun loadData(){
        runOnIO { (binding.leaderBoardList.adapter as LeaderBoardScreenAdapter).update(userdao.getAll())}
    }




}