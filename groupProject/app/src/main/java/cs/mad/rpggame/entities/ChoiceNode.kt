package cs.mad.rpggame.entities

import java.io.Serializable

data class ChoiceNode (
    var description: String,
    var buttonDescription: String?,
    var left: ChoiceNode? = null,
    var right: ChoiceNode? = null,
    var image: String?,
    var nodeNum: Int?
    ): Serializable
{

fun connectRightNode(node: ChoiceNode): ChoiceNode {
    this.right = node
    return node
}

fun connectLeftNode(node: ChoiceNode): ChoiceNode {
    this.left = node
    return node
}







}