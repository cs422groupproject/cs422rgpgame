package cs.mad.rpggame.activities

import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.room.Room
import cs.mad.rpggame.R
import cs.mad.rpggame.adapters.GameScreenAdapter
import cs.mad.rpggame.adapters.LoginScreenAdapter
import cs.mad.rpggame.database.UserDatabase
import cs.mad.rpggame.databinding.ActivityGameScreenBinding
import cs.mad.rpggame.entities.ChoiceNode
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO
import cs.mad.rpggame.runOnIO
import java.util.*

class GameScreen : AppCompatActivity() {
    private lateinit var binding: ActivityGameScreenBinding
    private lateinit var userdao: UserDAO
    private lateinit var player: MediaPlayer
    private lateinit var myTimer: CountDownTimer
    private val mediaPlayer = MediaPlayer()
    lateinit var mainHandler: Handler
    var time: Long = 0
    var isOn: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val db = Room.databaseBuilder(
            applicationContext,
            UserDatabase::class.java, UserDatabase.databaseName
        ).build()

        userdao = db.userDao()





      val url = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3"
//        player = MediaPlayer.create(this, Uri.parse(url))
        player = MediaPlayer.create(this, R.raw.the_old_tower_inn)





        val nulUser = User(null,"null",0,0,0,null)

        val user = intent.extras?.getSerializable("SelectedUser")as?User ?: nulUser


        binding.user.text = "User: " + user.name
        binding.attack.text = "Attack: " + user.attack
        binding.charisma.text = "Charisma: " + user.charisma
        binding.stealth.text = "Stealth: " + user.stealth

        binding.situtationList.adapter = GameScreenAdapter(user, userdao, this)





        binding.next.setOnClickListener{
            val intent = Intent(this, LeaderBoardScreen::class.java)
            startActivity(intent)
        }


        var finaleTime = 0

        myTimer = object: CountDownTimer(300000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
//                if (isOn == true){
                    time =  ((millisUntilFinished/1000))
//                finaleTime = time.toInt()
                    (binding.situtationList.adapter as GameScreenAdapter).updateTime(time)
                    binding.timeLimit.text = time.toString()
//                }

            }

            override fun onFinish() {
//                if (isOn == true){
//                    Log.d("description", "can you reach me?")
                    gotoLeaderBoards()
//                }

            }
        }




//        (binding.situtationList.adapter as GameScreenAdapter)




    }




    override fun onResume() {
        super.onResume()
//        isOn = true
//        time = 300000 / 1000
        myTimer.start()
        player.start()
        player.isLooping = true
    }

    override fun onPause() {
        super.onPause()
//        isOn = false
        player.pause()
        myTimer.cancel()

    }


    fun scrollingtoBottom(int : Int){
        val recycler = binding.situtationList
        recycler.smoothScrollToPosition(int)
    }


    fun gotoLeaderBoards(){
        val intent = Intent(this, LeaderBoardScreen::class.java)
        startActivity(intent)
    }







}