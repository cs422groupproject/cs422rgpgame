package cs.mad.rpggame.entities

import androidx.room.*
import java.io.Serializable


@Entity
data class User(@PrimaryKey var id: Long?,
                var name: String,
                var attack: Int,
                var stealth: Int,
                var charisma: Int,
                var time: Int? = null,
                var playerType: String? = null
                 ): Serializable{

}

@Dao
interface UserDAO{
    @Query("select * from User")
    suspend fun getAll(): List<User>

    @Insert
    suspend fun insert(user: User): Long

    @Insert
    suspend fun insertAll(user: List<User>)

    @Update
    suspend fun update(user: User)

    @Delete
    suspend fun delete(user: User)

}
