package cs.mad.rpggame.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.rpggame.R
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO
import java.util.*


class LeaderBoardScreenAdapter(val data: List<User>, val userDAO: UserDAO):  RecyclerView.Adapter<LeaderBoardScreenAdapter.ViewHolder>(){

    private val dataSet = mutableListOf<User>()

// quicksort algorithm created by Tony Hoare and used implementation from:
// https://www.geeksforgeeks.org/quick-sort/
    fun partition(arr: List<User>, low: Int, high: Int): Int {

        // pivot
        val pivot = arr.get(high).time

        // Index of smaller element and
        // indicates the right position
        // of pivot found so far
        var i = low - 1
        for (j in low..high - 1) {

            // If current element is smaller
            // than the pivot
            if (arr.get(j)?.time!! < pivot!!) {

                // Increment index of
                // smaller element
                i++
                Collections.swap(arr,i,j)
            }
        }
        Collections.swap(arr,i+ 1, high)
        return i + 1
    }

    fun quickSort(arr: List<User>, low: Int, high: Int) {
        if (low < high) {

            // pi is partitioning index, arr[p]
            // is now at right place
            val pi = partition(arr!!, low, high)

            // Separately sort elements before
            // partition and after partition
            quickSort(arr, low, pi - 1)
            quickSort(arr, pi + 1, high)
        }
    }

    init {
        this.dataSet.addAll(data)
        quickSort(dataSet,0,dataSet.size-1)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val userName: TextView = view.findViewById(R.id.name)
        val userTime: TextView = view.findViewById(R.id.time)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return LeaderBoardScreenAdapter.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.leader_board_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.userName.text = item.name
        holder.userTime.text = item.time.toString() + " seconds"

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }


    fun update(list: List<User>){
        list?.let{
            dataSet.addAll(it)
            quickSort(dataSet,0,dataSet.size-1)
            notifyDataSetChanged()
        }
    }











}


