package cs.mad.rpggame.activities


import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.room.Room
import cs.mad.rpggame.adapters.LoginScreenAdapter
import cs.mad.rpggame.database.UserDatabase
import cs.mad.rpggame.databinding.ActivityLoginScreenBinding
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO
import cs.mad.rpggame.runOnIO

// 3 character Types
//warrior (Atk: 6, stleath: 2, charisma: 2)
// Rogue (Atk: 2, stleath: 6, charisma: 2)
// Diplomat (Atk: 2, stleath: 2, charisma: 6)

// any time you want to use database within this class or adapter
// use runOnIO {} and place code within brackets
// hello

class LoginScreen : AppCompatActivity() {
    private lateinit var binding: ActivityLoginScreenBinding
    private lateinit var userdao: UserDAO


//create new reference to database  within each class that uses it i.e. (loginScren & LeaderBoardScren)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginScreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        val db = Room.databaseBuilder(
            applicationContext,
            UserDatabase::class.java, UserDatabase.databaseName
        ).build()

        userdao = db.userDao()

        //Enter empty list with dao
        binding.usersList.adapter = LoginScreenAdapter(listOf(), userdao)
        // update that list within adapter through this function
        loadData()















        binding.addUser.setOnClickListener {
        var user = User(null, "New Player",0,0,0,300)
            val name = showEditDialog(this, user)


//            loadData()

            //user = User(null, "New Player", 0,0,0,null)





            binding.usersList.smoothScrollToPosition((binding.usersList.adapter as LoginScreenAdapter).itemCount - 1)


        }
    }








    private fun showEditDialog(context: Context,  person:User):String {
        var item = person
        val customTitle = EditText(context)

        customTitle.setText(item.name)
        customTitle.textSize = 20 * context.resources.displayMetrics.scaledDensity

        AlertDialog.Builder(context)
            .setCustomTitle(customTitle)

            .setPositiveButton("Done") { _, _ ->
                    if (item.name !=  ""){
                        item.name = customTitle.text.toString()
                        runOnIO{ item.id = userdao.insert(item) }
                        (binding.usersList.adapter as LoginScreenAdapter).updateUI(item)

//                        item = User(null, item.name, 0, 0, 0, null)
//                        (binding.usersList.adapter as LoginScreenAdapter).update(mutableListOf(item))
//                        runOnIO { userdao.insert(item) }


                        }


                    //runOnIO


            }
              /*
            .setNegativeButton("Delete") { _,_ ->
                runOnIO { dao.delete(flashcard) }
                //dataSet.removeAt(position)
                //notifyItemRemoved(position)
            }

               */
            .create()
            .show()
        return item.name
    }


    private fun loadData(){
        runOnIO { (binding.usersList.adapter as LoginScreenAdapter).update(userdao.getAll().toMutableList())}

    }


}