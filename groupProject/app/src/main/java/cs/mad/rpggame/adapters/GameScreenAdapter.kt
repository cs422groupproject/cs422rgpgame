package cs.mad.rpggame.adapters

import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.ViewAnimator
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import cs.mad.rpggame.R
import cs.mad.rpggame.activities.GameScreen
import cs.mad.rpggame.activities.LeaderBoardScreen
import cs.mad.rpggame.entities.ChoiceNode
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO
import cs.mad.rpggame.runOnIO

class GameScreenAdapter(val user: User, val userDAO: UserDAO, val activity: GameScreen): RecyclerView.Adapter<GameScreenAdapter.ViewHolder>() {


    private val dataSet = mutableListOf<ChoiceNode>()
    private  val allNodes = mutableListOf<ChoiceNode>()
    private val node1 = ChoiceNode("Rummaging through a town of people it seems that I have lost him. The only thing going through my mind was the reprehensibly written ransom note. “You have 10 minutes to save your son or give me all the money you own.” Gripped with anger I looked onto the road and saw two roads in front of me. One is a shortcut and the other way takes longer. ", null, null, null,null,1)
    private val node2 = ChoiceNode("It seems that I have taken the longer path,on my way through the road  I came across a wild boar staring at me with anger in his eyes. My fear of boars have always been a relenting fear in my life, but too much is at stake. Alongside the boar I see another person. Do I dare get the bore’s attention to that person and have him be attacked or confront it myself.", "left road", null, null, null,2 )
    private val node3 = ChoiceNode("It seems you have taken the shorter path and thus you see the captor with your son making his way toward a secluded house so You run to that house before they can get there.", "right road", null, null, null,3 )
    private val node4 = ChoiceNode("The boar then came an attacked a killed the humble old man minding his own business. I can’t worry about ethics in a moment like this. Then I saw the captor with my son in his grasp. He was standing in a small house. Away from my son in the front. Overcome with adrilinin and options confused as to what should I do. ", "make boar attack other person", null, null, null,4 )
    private val node5 = ChoiceNode("I approached the boar myself filled with fear, and then my heart felt like it stopped as I see the captor approaching the boar with a sword in one hand and my son if the other. What should I do, is there any world in which I can reason with him, or do I just give him the money. ", "approach the boar myself", null, null, null,5 )
    private val node6 = ChoiceNode("As you hide behind the door of the front entrance you realize that you have 2 options here, do I attack the captor as soon as he enters the house by surprise, or do I grab my son as soon as I see him and run.", "hide inside the house", null, null, null,6)
    private val node7 = ChoiceNode("You are standing in front of the house, when the captor notices you and then stops in his tracks and says that he will kill your son right here and now if you don't give him the money he asked for. With nevers overcoming. You decide to…", "wait infront of the house ", null, null, null,7)
    private val node8 = ChoiceNode("node 8", "Attack the captor and kill him", null, null, null,8)
    private val node9 = ChoiceNode("node 9", "Stealthly get my son out of there", null, null, null,9)
    private val node10 = ChoiceNode("you give him your money and then he feeds you and your son to the wild boar. ", "will give him the money", null, null, null,10 )
    private val node11 = ChoiceNode("node 11", "will reason with him", null, null, null, 11 )
    private val node12 = ChoiceNode("You grab your son and leave as fast as you can thereby escaping and foiling the captors plans.", "grab son and bolt", null, null, null,12 )
    private val node13 = ChoiceNode("node 13", "Attack the captor", null, null, null,13 )
    private val node14 = ChoiceNode("You begin to beg for mercy on your knees asking the captor to please give your son back without giving him everything as that would leave your family with nothing. You begin to have tears in your eyes as emotions have overrun you. The captor watching then as a moment of mercy and asks you to give him merely half your money in return.", "beg for mercy", null, null, null,14 )
    private val node15 = ChoiceNode("you give the money to the captor and he immediately kills you and your son.", "give him the money", null, null, null,15 )
    private val node16 = ChoiceNode("you agree to give half of your money and then get your son back.", "you agree to give half your money", null, null, null,16 )
    private val node17 = ChoiceNode("the captor is angered by your response and has you and your son killed. ", "you refuse to give your money", null, null, null,17)
    private var time = 0

    init {
        // node1 choices
        this.node1.left = node2
        this.node1.right = node3
//
//        // node2 choices
        this.node2.left = node4
        this.node2.right = node5
//
//        // node3 choices
        this.node3.left = node6
        this.node3.right = node7
//
//        // node4 choices
        this.node4.left = node8
        this.node4.right = node9
//
//        // node5 choices
        this.node5.left = node10
        this.node5.right = node11
//
//        // node6 choices
        this.node6.left = node12
        this.node6.right = node13
//
//        // node7 choices
       this.node7.left = node14
        this.node7.right = node15
//
//        // node14 choices
        this.node14.left = node16
        this.node14.right = node17


        this.dataSet.add(node1)
        // ALL nodes List:
        this.allNodes.add(node1)
        this.allNodes.add(node2)
        this.allNodes.add(node3)
        this.allNodes.add(node4)
        this.allNodes.add(node5)
        this.allNodes.add(node6)
        this.allNodes.add(node7)
        this.allNodes.add(node8)
        this.allNodes.add(node9)
        this.allNodes.add(node10)
        this.allNodes.add(node11)
        this.allNodes.add(node12)
        this.allNodes.add(node13)
        this.allNodes.add(node14)
        this.allNodes.add(node15)
        this.allNodes.add(node16)
        this.allNodes.add(node17)

    }









    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var description: TextView = view.findViewById(R.id.description)
        var choice1 : Button = view.findViewById(R.id.choice_one)
        var choice2 : Button = view.findViewById(R.id.choice_two)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.game_story_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.description.text = item.description
        holder.choice1.text = item.left?.buttonDescription
        holder.choice2.text = item.right?.buttonDescription
        val lastNode = dataSet[dataSet.size-1]
        val left = lastNode?.left
        val right = lastNode?.right





        holder.choice1.setOnClickListener{
           val clickedNodeNum =  (holder.choice1.text as String?)?.let { it1 -> nodeClicked(allNodes, it1) }
            val newItem = lastNode.left



            if (clickedNodeNum != null) {

                if (clickedNodeNum == newItem?.nodeNum!!){



            if (newItem?.left != null && newItem?.right != null){
                dataSet.add(newItem ?: ChoiceNode("null",null,null,null,null,null))

                notifyDataSetChanged()
                activity.scrollingtoBottom(dataSet.size-1)

            }

            else{

                if (newItem?.nodeNum == 12 || newItem?.nodeNum == 16){
                    winGame(newItem,it.context)
                }

                else if (newItem?.nodeNum == 10){
                    LoseGame(newItem,it.context)
                }

                else if (newItem?.nodeNum == 8){
                    if (user.attack >= 6){
                        newItem.description = "You win the game, you were able to stab the captor and bludgeoned him to death and then rescued your son with blood stained hands thereby scaring your son who will need therapy for the rest of their life. "
                        winGame(newItem,it.context)
                    }
                    else{
                        // update attack value
                        user?.attack += 1
                         runOnIO { userDAO.update(user)}
                        newItem.description = "You Lose, your attack was not strong enough to overcome the captor resulting in him taking your money and then feeding your son to that wild boar who you realize was sent by the captor himself."
                        LoseGame(newItem,it.context)
                    }

                }

//                val intent = Intent(it.context, LeaderBoardScreen::class.java)
//                startActivity(it.context, intent)

            }

        }
    }



        }



        holder.choice2.setOnClickListener{
            val clickedNodeNum =  (holder.choice2.text as String?)?.let { it1 -> nodeClicked(allNodes, it1) }
            val newItem = lastNode.right

//            clickedNodeNum == newItem?.nodeNum!!
//            holder.choice1.isEnabled = false
            if (clickedNodeNum != null) {
                if (clickedNodeNum == newItem?.nodeNum!!){
//                    holder.choice2.isEnabled = false



                if (newItem?.left != null && newItem?.right != null){
                    dataSet.add(newItem ?: ChoiceNode("null",null,null,null,null,null))
                    notifyDataSetChanged()
                    activity.scrollingtoBottom(dataSet.size-1)
                }

                else {


                    if (newItem?.nodeNum == 15 || newItem?.nodeNum == 17){
                        LoseGame(newItem,it.context)
                    }

                    else if (newItem?.nodeNum == 9 ){
                        if (user?.stealth >= 6){
                            newItem.description = "you were able to successfully sneak into the house and rescue your son without alarming the captor to which you and your son escaped. "
                            winGame(newItem,it.context)
                        }
                        else{
                            // update stealth value
                            user?.stealth +=1
                             runOnIO { userDAO.update(user)}
                            newItem.description = "you were not quiet enough during your way toward your son and the captor then feeds you and your son to the wild boar to which you realize was sent by the captor himself. "
                            LoseGame(newItem,it.context)
                        }

                    }

                    else if (newItem?.nodeNum == 11){
                        if (user?.charisma >= 6){
                            newItem.description = "You were able to give such a convincing speech that the captor felt so moved he returned your son and dropped to his knees apologizing for his transgression."
                            winGame(newItem,it.context)
                        }
                        else{
                            // update charisma value
                            user?.charisma +=1
                             runOnIO { userDAO.update(user)}
                            newItem.description = "The captor laughs in your face and reminds you this is not an after school special and then proceeds to feed you and your son to the boar."
                            LoseGame(newItem,it.context)
                        }

                    }

                    else if (newItem?.nodeNum == 13){
                        if (user.attack >= 6){
                            newItem.description = "You were successfully able to attack the captor and kill him in front of your son thereby scaring your son who will need therapy for the rest of their life. "
                            winGame(newItem,it.context)
                        }
                        else{
                            // update attack value
                            user?.attack += 1
                          runOnIO { userDAO.update(user)}
                            newItem.description = "your attack was unsuccessful and the captor was able to overpower you. "
                            LoseGame(newItem,it.context)
                        }
                    }



                }


        }
    }


        }
    }

    override fun getItemCount(): Int {
       return dataSet.size
    }


   fun winGame(newItem: ChoiceNode, context: Context){
       updateUserDatabaseTime(time)
       AlertDialog.Builder(context).setTitle("You WIN!")
           .setMessage(newItem?.description)
           .setPositiveButton("Done") { _,_ ->
               context.startActivity(Intent(context, LeaderBoardScreen::class.java))
           }
           .create()
           .show()

   }

    fun LoseGame(newItem: ChoiceNode, context: Context){
        updateUserDatabaseTime(300)
        AlertDialog.Builder(context).setTitle("You LOSE!")
            .setMessage(newItem?.description)
            .setPositiveButton("Done") { _,_ ->
                context.startActivity(Intent(context, LeaderBoardScreen::class.java))
            }
            .create()
            .show()
    }


    fun nodeClicked (allNodes: List<ChoiceNode>, des: String): Int{
        var returnVal = 0
//        Log.d("description", des)
//        Log.d("seperation", "***************************************")
        for (i in allNodes.indices) {
            val node = allNodes[i]
            if (node.buttonDescription.equals(des)) {
//                node.buttonDescription?.let { Log.d("buttonDescription", it) }
//                Log.d("description", des)
//                Log.d("returnVal", node.nodeNum.toString())
                returnVal = node.nodeNum ?: -1
                break
            }
        }

        return  returnVal
    }



    fun addItem(it: ChoiceNode) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }



    fun updateTime(currentTime: Long){
        time = 300 - currentTime.toInt()
    }


    fun updateUserDatabaseTime(newTime: Int){
        if (user.time!! > newTime){
            user.time = newTime
            Log.d("Log to database", user.time.toString())
            runOnIO { userDAO.update(user)}

        }
    }


}
