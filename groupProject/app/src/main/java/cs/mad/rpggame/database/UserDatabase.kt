package cs.mad.rpggame.database

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO


@Database(entities = [User::class], version = 1)
abstract class UserDatabase: RoomDatabase() {

    companion object{
        const val databaseName = "User_Database"
    }

    abstract fun userDao(): UserDAO


}