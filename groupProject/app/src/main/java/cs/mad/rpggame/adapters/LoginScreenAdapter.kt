package cs.mad.rpggame.adapters


import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO
import cs.mad.rpggame.R
import cs.mad.rpggame.activities.LoginScreen
import cs.mad.rpggame.activities.LoginScreen2
//import cs.mad.rpggame.databinding.ActivityLoginScreenBinding
import cs.mad.rpggame.runOnIO


class LoginScreenAdapter(val data: List<User>, val userDAO: UserDAO): RecyclerView.Adapter<LoginScreenAdapter.ViewHolder>()  {
    private var dataSet = listOf(User(null, "",0,0,0, null)).toMutableList()
    //private var dataSet = listOf<User>(data)data.toMutableList()


    init {
        this.dataSet.addAll(data)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
       // val usersList:RecyclerView = view.findViewById(R.id.usersList)
      val user:TextView = view.findViewById(R.id.uname)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // can delete this (these values are just for my testing purposes)
        //val vh = ViewHolder(parent)



        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.login_item, parent, false)
        )




    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.user.text = item.name
       //holder.users?.text ?:  item.name
        
        val intent = Intent(holder.itemView.context, LoginScreen2::class.java)
        intent.putExtra("SelectedUser", item)
       holder.itemView.setOnClickListener{




            holder.itemView.context.startActivity(intent)

        }


    /*
        holder.itemView.setOnClickListener {


        //val toast = Toast.makeText(this, item.attack.toString(), Toast.LENGTH_LONG)
            //toast.show()
        }

     */
       // intent.putExtra("selectedUser",  user)

















    }

    override fun getItemCount(): Int {
        return dataSet.size;
    }

/*
    fun update(list: List<User>){
        list?.let{
            dataSet.addAll(it)
            notifyDataSetChanged()
        }
    }

 */
    fun update(it: List<User>){
        runOnIO {
            dataSet.addAll(it)
        }
        notifyItemInserted(dataSet.lastIndex)
    }

    fun updateUI(user: User){
        dataSet.add(user)
        notifyDataSetChanged()
    }


}