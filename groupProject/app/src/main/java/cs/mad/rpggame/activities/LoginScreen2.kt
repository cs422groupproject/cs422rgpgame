package cs.mad.rpggame.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.room.Room
import cs.mad.rpggame.adapters.LoginScreenAdapter
import cs.mad.rpggame.database.UserDatabase
import cs.mad.rpggame.databinding.ActivityLoginScreen2Binding
//import cs.mad.rpggame.databinding.ActivityLoginScreenBinding
import cs.mad.rpggame.entities.User
import cs.mad.rpggame.entities.UserDAO
import cs.mad.rpggame.runOnIO



// 3 character Types
//warrior (Atk: 6, stleath: 2, charisma: 2)
// Rogue (Atk: 2, stleath: 6, charisma: 2)
// Diplomat (Atk: 2, stleath: 2, charisma: 6)



// any time you want to use database within this class or adapter
// use runOnIO {} and place code within brackets

class LoginScreen2 : AppCompatActivity() {
    private lateinit var binding: ActivityLoginScreen2Binding
    private lateinit var userdao: UserDAO



//create new reference to database  within each class that uses it i.e. (loginScren & LeaderBoardScren)

    override fun onCreate(savedInstanceState: Bundle?) {
        var job:String? = ""

        super.onCreate(savedInstanceState)
        binding = ActivityLoginScreen2Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



        val db = Room.databaseBuilder(
            applicationContext,
            UserDatabase::class.java, UserDatabase.databaseName
        ).build()

        userdao = db.userDao()
        /*
        //Enter empty list with dao
        binding.usersList.adapter = LoginScreenAdapter(listOf(), userdao)
        // update that list within adapter through this function
        loadData()




        binding.uname.setOnClickListener{

        }


         */

        val nulUser = User(null, "", 0,0,0,null)

        val  user = intent.extras?.getSerializable("SelectedUser")as?User ?:nulUser


        val intent = Intent(this, GameScreen::class.java)




        binding.Button.setOnClickListener{
            val job = "Warrior"
            //person = User(null, userName, 6,2,2,null)
            if (user.playerType == null || user?.playerType != job){
                user.playerType = job
                user.attack = 6
                user.stealth = 2
                user.charisma = 2
            }

            intent.putExtra("SelectedUser", user)
            startActivity(intent)
        }

        binding.Button2.setOnClickListener{
            val job = "Rogue"
            if (user.playerType == null || user.playerType != job){
                user.playerType = job
                user.stealth = 6
                user.attack = 2
                user.charisma = 2
            }

            intent.putExtra("SelectedUser", user)
            startActivity(intent)
        }

        binding.Button3.setOnClickListener{
            val job = "Diplomat"
            if (user.playerType == null || user.playerType !=  job){
                user.playerType = job
                user.charisma = 6
                user.attack = 2
                user.stealth = 2
            }

            intent.putExtra("SelectedUser", user)
            startActivity(intent)
        }

    }


        /*
        binding.addUser.setOnClickListener{

            var count = (binding.usersList.adapter as LoginScreenAdapter).itemCount
            val user = User(null, "kdkdkdkdkdkdkdkdkdkdkdkdkdd $count",3,2, 3,null)
            //Profile $count
            //val toast = Toast.makeText(this,user.name, Toast.LENGTH_LONG )
            //toast.show()
            //binding.uname.text = user.name


            //list.toMutableList().add(user)
            //list.toMutableList().add(user)
            (binding.usersList.adapter as LoginScreenAdapter).update(listOf(user))
            runOnIO {
                userdao.insert(user)
            }




            binding.usersList.smoothScrollToPosition((binding.usersList.adapter as LoginScreenAdapter).itemCount - 1)
            // var list:List<User> = listOf(User(null, "Sirath",2,2,0, time = null))

            // list.toMutableList().add(user)
            //(binding.usersList.adapter as LoginScreenAdapter).update(list)
            //binding.usersList.smoothScrollToPosition((binding.usersList.adapter as LoginScreenAdapter).itemCount - 1)
        }


            binding.next.setOnClickListener{
                // dont actually give id a value just insert null, database will automatically asign num
                val person = User(null, "john",3,2, 3,null)
                val intent = Intent(this, GameScreen::class.java)
              intent.putExtra("selectedUser",  person)
                startActivity(intent)

            }



    }




    private fun loadData(){
        runOnIO { (binding.usersList.adapter as LoginScreenAdapter).update(userdao.getAll().toMutableList())}
*/
    }


